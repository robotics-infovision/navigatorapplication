package com.example.myapplication

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.firebase.messaging.FirebaseMessaging

class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {

    private val TAG = "mFirebaseIIDService"
    val SUBSCRIBE_TO = "userABC"

    override fun onTokenRefresh() {
        /**
         * @author: Vivek K Bhati
         * @description: Get updated InstanceID token.
         * @param: NA
         * @return: NA
         */
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.d(TAG, "Refreshed token: $refreshedToken")

        /**
         * @author: Vivek K Bhati
         * @description: Once the token is generated, subscribe to topic with the userId
         * @param: NA
         * @return: NA
         */
        FirebaseMessaging.getInstance().subscribeToTopic(SUBSCRIBE_TO)
        sendRegistrationToServer(refreshedToken)
    }

    private fun sendRegistrationToServer(refreshedToken: String?) {}
}
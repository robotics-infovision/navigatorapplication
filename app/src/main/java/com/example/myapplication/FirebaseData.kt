package com.example.myapplication

class FirebaseData(s: String?, s1: String?, s2: String?) {
    private var title: String? = null
    private var message: String? = null
    private var name: String? = null


    fun FirebaseData(title: String?, message: String?, name: String?) {
        this.title = title
        this.message = message
        this.name = name
    }

    fun getTitle(): String? {
        return this.title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getMessage(): String? {
        return this.message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getName(): String? {
        return this.name
    }

    fun setName(name: String?) {
        this.name = name
    }
}

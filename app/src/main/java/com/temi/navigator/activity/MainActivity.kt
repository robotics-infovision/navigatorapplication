package com.temi.navigator.activity

import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessaging
import com.robotemi.sdk.Robot
import com.robotemi.sdk.TtsRequest
import com.robotemi.sdk.constants.ContentType
import com.robotemi.sdk.face.ContactModel
import com.robotemi.sdk.face.OnFaceRecognizedListener
import com.robotemi.sdk.listeners.*
import com.robotemi.sdk.model.DetectionData
import com.robotemi.sdk.navigation.listener.OnDistanceToLocationChangedListener
import com.robotemi.sdk.permission.OnRequestPermissionResultListener
import com.robotemi.sdk.permission.Permission
import com.temi.navigation.R
import com.temi.navigator.constants.Constants
import com.temi.navigator.constants.Constants.Companion.REQUEST_CODE_FACE_START
import com.temi.navigator.constants.Constants.Companion.REQUEST_CODE_FACE_STOP
import com.temi.navigator.constants.Constants.Companion.REQUEST_CODE_GET_MAP_LIST
import com.temi.navigator.constants.Constants.Companion.REQUEST_CODE_MAP
import com.temi.navigator.constants.Constants.Companion.REQUEST_CODE_SEQUENCE_FETCH_ALL
import com.temi.navigator.constants.Constants.Companion.REQUEST_CODE_SEQUENCE_PLAY
import com.temi.navigator.constants.Constants.Companion.REQUEST_CODE_SEQUENCE_PLAY_WITHOUT_PLAYER
import com.temi.navigator.constants.Constants.Companion.REQUEST_CODE_START_DETECTION_WITH_DISTANCE
import com.temi.navigator.model.FirebaseData
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity(),
    OnDistanceToLocationChangedListener,
    Robot.TtsListener,
    OnConversationStatusChangedListener,
    OnGoToLocationStatusChangedListener,
    OnFaceRecognizedListener,
    OnDetectionStateChangedListener,
    OnDetectionDataChangedListener,
    OnUserInteractionChangedListener,
    Robot.AsrListener,
    OnGreetModeStateChangedListener,
    OnRequestPermissionResultListener {
    //fcm server settings. Do not alter.
    var TOPIC: String? = null
    private var FCM_token: String? = null
    private val FCM_API: String = Constants.C_FCM_API
    private val FBSKey: String = Constants.C_FBSKey
    private val serverKey: String = Constants.C_serverKey
    private val contentType: String = Constants.C_contentType
    val TAG: String = Constants.C_TAG
    var status = 0
    var navigating: String = OnGoToLocationStatusChangedListener.COMPLETE

    // JSON data
    var NOTIFICATION_TITLE: String? = null
    var NOTIFICATION_MESSAGE: String? = null
    var NOTIFICATION_NAME: String? = null

    //initial data
    private var key: String = Constants.C_key
    private var name: String = Constants.C_name
    private var message: String = Constants.C_charging_location
    private var robot: Robot? = null

    // TTL_Request ID
    private var ttsSpeakGreet: String? = null
    private var ttsSpeakInvalid: String? = null
    private var ttsLocationGreet: String? = null
    private var ttsLocationInvalid: String? = null
    private var ttsCallInvalid: String? = null
    private var ttsLocationCompleted: String? = null
    private var ttsLocationCompletedConsultation: String? = null
    private var ttsLocationNurse: String? = null
    private var ttsLocationCompletedSpeak: String? = null
    private var ttsAsrUserLocation: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Initializing temi robot instance
        robot = Robot.getInstance()
        robot?.setKioskModeOn(true)
//        robot?.hideTopBar()
        robot?.goTo(getString(R.string.location_1))
        robot?.detectionModeOn
//        robot?.setDetectionModeOn(true,0.5F)
        var contact1: ContactModel
        var content1: ContentType

        //Initializing Firebase
        FirebaseApp.initializeApp(this)
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        FirebaseMessaging.getInstance().subscribeToTopic(getString(R.string.C_subscribed_topic_fcm))

        //Getting tag and putting in log
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult: InstanceIdResult ->
            FCM_token = instanceIdResult.token
            Log.d(TAG, getString(R.string.fcm_token_log_message).toString() + FCM_token)
        }
    }

    //function called when notification is recieved
    private fun notificationRecieved() {
        TOPIC =
            getString(R.string.C_send_notification_topic_fcm) //topic has to match what the receiver subscribed to
        NOTIFICATION_TITLE = getString(R.string.notification_reply_initials) + getKey()
        NOTIFICATION_MESSAGE = getMessage()
        NOTIFICATION_NAME = getName()
        val notification = JSONObject()
        val notifcationBody = JSONObject()
        try {
            notifcationBody.put(getString(R.string.key_title), NOTIFICATION_TITLE)
            notifcationBody.put(getString(R.string.key_message), NOTIFICATION_MESSAGE)
            notifcationBody.put(getString(R.string.key_name), NOTIFICATION_NAME)
            notification.put(getString(R.string.key_to), TOPIC)
            notification.put(getString(R.string.key_data), notifcationBody)
        } catch (e: JSONException) {
            Log.e(TAG, getString(R.string.log_onCreate) + e.message)
        }
        sendNotification(notification)
        hideSoftKeyboard()
        closeKeyBoard()
    }

    //function called when notification is to be sent
    private fun sendNotification(notification: JSONObject) {
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
            Response.Listener { response ->
                Log.i(TAG, getString(R.string.log_response) + response.toString())
                NOTIFICATION_TITLE = ""
                NOTIFICATION_MESSAGE = ""
                NOTIFICATION_NAME = ""
            },
            Response.ErrorListener {
                Toast.makeText(
                    this,
                    getString(R.string.toast_request_error),
                    Toast.LENGTH_LONG
                ).show()
                Log.i(TAG, getString(R.string.log_response_error))
            }) {
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params[getString(R.string.C_authorization)] = serverKey
                params[getString(R.string.C_content)] = contentType
                return params
            }
        }
//        MySingleton().getInstance(this)?.addToRequestQueue(jsonObjectRequest)
        hideSoftKeyboard()
        closeKeyBoard()
    }

    // From MyFirebaseMessagingService EventBus
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun getNotificationData(data: FirebaseData) {
        this.setMessage("reception")
        data.getTitle()?.let { this.setKey(it) }
        data.getMessage()?.let { this.setMessage(it) }
        data.getName()?.let { this.setName(it) }
        work(getKey(), getMessage(), getName())
        notificationRecieved()
    }

    //function to hide keyboard
    fun hideSoftKeyboard() {
        val inputMethodManager = this.getSystemService(
            INPUT_METHOD_SERVICE
        ) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(
                this.currentFocus!!.windowToken,
                0
            )
        }
    }

    private fun closeKeyBoard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun getKey(): String {
        return key
    }

    fun setKey(key: String) {
        this.key = key
    }

    fun getName(): String {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getMessage(): String {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    //function called after receiving data commands from pepper
    fun work(key: String?, message: String?, name: String?) {
        when {
            key == null -> {
                robot!!.goTo(getString(R.string.location_1), true, null, null)
            }
            key.equals(getString(R.string.key_speak), ignoreCase = true) -> {
                when {
                    message != null && name != null -> {
                        val ttsRequest = TtsRequest.create(
                            getString(R.string.speak_greet_Hey) +
                                    " " + name
                                    + message,
                            true, TtsRequest.Language.EN_US
                        )
                        ttsSpeakGreet = ttsRequest.id.toString()
                        robot!!.speak(ttsRequest)
                    }
                    else -> {
                        val ttsRequest = TtsRequest.create(
                            getString(R.string.speak_notification_error),
                            true, TtsRequest.Language.EN_US
                        )
                        ttsSpeakInvalid = ttsRequest.id.toString()
                        robot!!.speak(ttsRequest)
                    }
                }
            }
            key.equals(getString(R.string.key_location), ignoreCase = true) -> {
                when {
                    message != null && name != null -> {
                        val ttsRequest = TtsRequest.create(
                            (getString(R.string.speak_greet_Hey) +
                                    " " + name + ", "
                                    + getString(R.string.speak_follow_me) + " "
                                    + getString(R.string.speak_will_take_you) + " " +
                                    message + "."),
                            true, TtsRequest.Language.EN_US
                        )
                        ttsLocationGreet = ttsRequest.id.toString()
                        robot!!.speak(ttsRequest)
                        Log.i("status", ttsRequest.component4().toString())
                    }
                    else -> {
                        val ttsRequest = TtsRequest.create(
                            getString(R.string.speak_incorrect_location),
                            true, TtsRequest.Language.EN_US
                        )
                        ttsLocationInvalid = ttsRequest.id.toString()
                        Robot.getInstance().speak(ttsRequest)
                    }
                }
            }
            key.equals(getString(R.string.call), ignoreCase = true) -> {
                when {
                    message.equals(getString(R.string.jigar), ignoreCase = true) -> {
                        robot!!.startTelepresence(
                            getString(R.string.doctor_jigar),
                            getString(R.string.jigar_call_id)
                        )
                    }
                    message.equals(getString(R.string.shubham), ignoreCase = true) -> {
                        robot!!.startTelepresence(
                            getString(R.string.doctor_shubham),
                            getString(R.string.shubham_call_id)
                        )
                    }
                    else -> {
                        Log.i(getString(R.string.member_model), robot!!.allContact.toString())
                    }
                }
            }
            else -> {
                val ttsRequest = TtsRequest.create(
                    getString(R.string.invalid_command_call),
                    true, TtsRequest.Language.EN_US
                )
                ttsCallInvalid = ttsRequest.id.toString()
                Robot.getInstance().speak(ttsRequest)
            }
        }
    }

    fun wait(ms: Int) {
        try {
            Thread.sleep(ms.toLong())
        } catch (ex: InterruptedException) {
            Thread.currentThread().interrupt()
        }
    }

    //function called continuously when Temi is moving
    override fun onDistanceToLocationChanged(distances: Map<String, Float>) {
//        Log.i("location and distance", currentDistance.toString())
//        for (location: String in distances.keys) {
//            currentDistance[location] = distances[location]
//        }
    }

    override fun onTtsStatusChanged(ttsRequest: TtsRequest) {
        val tag = "onTtsStatusChanged"
        Log.d(tag, "Id = " + ttsRequest.id)
        Log.d(tag, "Speech = " + ttsRequest.speech)
        Log.d(tag, "Status = " + ttsRequest.status)
        when {
            (ttsSpeakGreet != null) && ttsRequest.id.toString()
                .equals(ttsSpeakGreet, ignoreCase = true)
                    && (ttsRequest.status == TtsRequest.Status.COMPLETED) -> {
                Log.d(tag, "Status = Speak greeting" + ttsRequest.status)
            }
            (ttsSpeakInvalid != null) && ttsRequest.id.toString()
                .equals(ttsSpeakInvalid, ignoreCase = true)
                    && (ttsRequest.status == TtsRequest.Status.COMPLETED) -> {
                Log.d(tag, "Status = Speak invalid" + ttsRequest.status)
            }
            (ttsLocationGreet != null) && ttsRequest.id.toString()
                .equals(ttsLocationGreet, ignoreCase = true)
                    && (ttsRequest.status == TtsRequest.Status.COMPLETED) -> {
                Log.d(tag, "Status = Location greeting status: " + ttsRequest.status)
                robot!!.goTo(getMessage(), true, null, null)
            }
            (ttsLocationInvalid != null) && ttsRequest.id.toString()
                .equals(ttsLocationInvalid, ignoreCase = true)
                    && (ttsRequest.status == TtsRequest.Status.COMPLETED) -> {
                Log.d(tag, "Status = Location Invalid status: " + ttsRequest.status)
            }
            (ttsLocationCompleted != null) && ttsRequest.id.toString()
                .equals(ttsLocationCompleted, ignoreCase = true)
                    && (ttsRequest.status == TtsRequest.Status.COMPLETED) -> {
                Log.d(tag, "Status = Location completed status: " + ttsRequest.status)
                val ttsRequest1 = TtsRequest.create(
                    getString(R.string.going_to_reception),
                    true, TtsRequest.Language.EN_US
                )
                ttsLocationCompletedSpeak = ttsRequest1.id.toString()
                Robot.getInstance().speak(ttsRequest1)
            }
            (ttsLocationCompletedConsultation != null) && ttsRequest.id.toString()
                .equals(ttsLocationCompletedConsultation, ignoreCase = true)
                    && (ttsRequest.status == TtsRequest.Status.COMPLETED) -> {
                Log.d(tag, "Status = Location consultation status: " + ttsRequest.status)
                val ttsRequest1 = TtsRequest.create(
                    getString(R.string.nurse_assist),
                    true, TtsRequest.Language.EN_US
                )
                ttsLocationNurse = ttsRequest1.id.toString()
                Robot.getInstance().speak(ttsRequest1)
            }
            (ttsLocationNurse != null) && ttsRequest.id.toString()
                .equals(ttsLocationNurse, ignoreCase = true)
                    && (ttsRequest.status == TtsRequest.Status.COMPLETED) -> {
                Log.d(tag, "Status = Location nurse status: " + ttsRequest.status)
                Handler().postDelayed({
                    val intent = Intent()
                    //            intent.action=""
                    intent.component = ComponentName(
                        "com.vivek.activities",
                        "com.vivek.activities.basicvideochat.activity.UIMainActivity"
                    )
                    if (intent.resolveActivity(packageManager) != null) {
                        startActivity(intent)
                    }
                }, 2000)
            }
            (ttsLocationCompletedSpeak != null) && ttsRequest.id.toString()
                .equals(ttsLocationCompletedSpeak, ignoreCase = true)
                    && (ttsRequest.status == TtsRequest.Status.COMPLETED) -> {
                Log.d(tag, "Status = Location nurse speak status: " + ttsRequest.status)
                setMessage(getString(R.string.location_1))
                robot!!.goTo(getString(R.string.location_1))
            }
            (ttsAsrUserLocation != null) && ttsRequest.id.toString()
                .equals(ttsAsrUserLocation, ignoreCase = true)
                    && (ttsRequest.status == TtsRequest.Status.COMPLETED) -> {
                Log.d(tag, "Status = Location ASR Result Location Status: " + ttsRequest.status)
                robot?.setDetectionModeOn(on = true, 0.5F)
                robot!!.goTo(this.getMessage(), true, null, null)
            }
        }
    }

    override fun onConversationStatusChanged(i: Int, s: String) {
        status = i
    }

    override fun onGoToLocationStatusChanged(
        location: String,
        status: String,
        descriptionId: Int,
        description: String
    ) {
        Log.i(
            "onGoToLocationStatusChanged: ",
            "location:" + location +
                    "--status:" + status +
                    "--descriptionId:" + descriptionId +
                    "--description:" + description
        )

        //to check the navigation status of temi
        navigating = status

        if (status == OnGoToLocationStatusChangedListener.COMPLETE.toString()
            && location != "reception"
            && location != "home base"
        ) {
//            Handler().postDelayed({
            if (!location.equals(getString(R.string.location_7), ignoreCase = true)) {
                val ttsRequest: TtsRequest = TtsRequest.create(
                    (getString(R.string.speak_greet_drop_off).toString() + " "
                            + getName() + getString(R.string.speak_status) + " " + getMessage()
                            + getString(R.string.speak_thank_you)),
                    true, TtsRequest.Language.EN_US
                )
                ttsLocationCompleted = ttsRequest.id.toString()
                robot!!.speak(ttsRequest)
            } else {
                val ttsRequest: TtsRequest = TtsRequest.create(
                    (getString(R.string.speak_greet_drop_off).toString() + " "
                            + getName() + getString(R.string.speak_status) + " " + getMessage()
                            + getString(R.string.speak_thank_you)),
                    true, TtsRequest.Language.EN_US
                )
                ttsLocationCompletedConsultation = ttsRequest.id.toString()
                robot!!.speak(ttsRequest)
            }
//            }, 2000)
        }
    }

    override fun onFaceRecognized(contactModelList: List<ContactModel>) {
        Log.i("onFaceRecognized", contactModelList.toString())
        if (contactModelList.isEmpty()) {
            return
        }

        val imageKey = contactModelList.find { it.imageKey.isNotBlank() }?.imageKey
        if (!imageKey.isNullOrBlank()) {
            showFaceRecognitionImage(imageKey)
        } else {
//            imageViewFace.setImageResource(R.drawable.app_icon)
//            imageViewFace.visibility = View.VISIBLE
        }

        for (contactModel in contactModelList) {
            if (contactModel.userId.isBlank()) {
                Toast.makeText(this, "onFaceRecognized: Unknown face", Toast.LENGTH_LONG)
            } else {
                Toast.makeText(
                    this,
                    "onFaceRecognized: ${contactModel.firstName} ${contactModel.lastName}",
                    Toast.LENGTH_LONG
                )
            }
        }
    }

    private val executorService = Executors.newSingleThreadExecutor()

    private fun showFaceRecognitionImage(mediaKey: String) {
        if (mediaKey.isEmpty()) {
//            imageViewFace.setImageResource(R.drawable.app_icon)
//            imageViewFace.visibility = View.GONE
            return
        }
        executorService.execute {
            val inputStream =
                robot?.getInputStreamByMediaKey(ContentType.FACE_RECOGNITION_IMAGE, mediaKey)
                    ?: return@execute
            runOnUiThread {
//                imageViewFace.visibility = View.VISIBLE
//                imageViewFace.setImageBitmap(BitmapFactory.decodeStream(inputStream))
                try {
                    inputStream.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onDetectionStateChanged(state: Int) {
        Log.i("onDetectionStateChanged: ", "Detection State is changed: $state")
        when (state) {
            2 -> {
                if (this.getMessage() == "reception" && navigating == OnGoToLocationStatusChangedListener.COMPLETE) {
//                    robot?.startFaceRecognition()
                    Log.i("onDetectionStateChanged: ", "Detection State Detected ASR: $state")
                    robot?.askQuestion("Where would you like to go?")
                }
                if (this.getMessage() != "reception" && navigating != OnGoToLocationStatusChangedListener.COMPLETE) {
                    robot?.goTo(this.getMessage(), true, null, null)
                }
            }
            1 -> {
//                robot?.stopFaceRecognition()
                if (this.getMessage() != "reception") {
                    robot?.stopMovement()
                }
            }
            0 -> {
            }
        }
    }

    override fun onDetectionDataChanged(detectionData: DetectionData) {
//        Log.i("onDetectionDataChanged: ","Detection data is changed-- distance: ${detectionData.distance} and status: ${detectionData.isDetected}")
    }

    override fun onStop() {
        robot?.removeOnDistanceToLocationChangedListener(this)
        robot?.removeTtsListener(this)
        robot?.removeOnConversationStatusChangedListener(this)
        robot?.removeOnGoToLocationStatusChangedListener(this)
        robot?.removeOnFaceRecognizedListener(this)
        if (robot?.checkSelfPermission(Permission.FACE_RECOGNITION) == Permission.GRANTED) {
            robot?.stopFaceRecognition()
        }
        robot?.removeOnDetectionStateChangedListener(this)
        robot?.removeOnDetectionDataChangedListener(this)
        robot?.removeOnUserInteractionChangedListener(this)
        robot?.removeAsrListener(this)
        robot?.removeOnGreetModeStateChangedListener(this)
        robot?.removeOnRequestPermissionResultListener(this)
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onStart() {
        super.onStart()
        robot?.addOnFaceRecognizedListener(this)
        robot?.addOnGoToLocationStatusChangedListener(this)
        robot?.addTtsListener(this)
        robot?.addOnConversationStatusChangedListener(this)
        robot?.addOnDistanceToLocationChangedListener(this)
        robot?.addOnDetectionStateChangedListener(this)
        robot?.addOnDetectionDataChangedListener(this)
        robot?.addOnUserInteractionChangedListener(this)
        robot?.addAsrListener(this)
        robot?.addOnGreetModeStateChangedListener(this)
        robot?.addOnRequestPermissionResultListener(this)
        EventBus.getDefault().register(this)
    }

    override fun onRequestPermissionResult(
        permission: Permission,
        grantResult: Int,
        requestCode: Int
    ) {
        val log = String.format("Permission: %s, grantResult: %d", permission.value, grantResult)
        if (grantResult == Permission.DENIED) {
//            imageViewFace.visibility = View.GONE
            return
        }
        when (permission) {
            Permission.FACE_RECOGNITION -> if (requestCode == REQUEST_CODE_FACE_START) {
                robot?.startFaceRecognition()
            } else if (requestCode == REQUEST_CODE_FACE_STOP) {
                robot?.stopFaceRecognition()
            }
            Permission.SEQUENCE -> when (requestCode) {
                REQUEST_CODE_SEQUENCE_FETCH_ALL -> {
//                    getAllSequences()
                }
                REQUEST_CODE_SEQUENCE_PLAY -> {
//                    playFirstSequence(true)
                }
                REQUEST_CODE_SEQUENCE_PLAY_WITHOUT_PLAYER -> {
//                    playFirstSequence(false)
                }
            }
            Permission.MAP -> if (requestCode == REQUEST_CODE_MAP) {
//                getMap()
            } else if (requestCode == REQUEST_CODE_GET_MAP_LIST) {
//                getMapList()
            }
            Permission.SETTINGS -> if (requestCode == REQUEST_CODE_START_DETECTION_WITH_DISTANCE) {
//                startDetectionWithDistance()
            }
            else -> {
                // no-op
            }
        }
    }

    override fun onUserInteraction(isInteracting: Boolean) {
        Log.i("onUserInteraction: ", "user is: $isInteracting")
    }

    override fun onAsrResult(asrResult: String) {
        when {
            asrResult.toLowerCase(Locale.getDefault()).contains(getString(R.string.location_2))
                    || asrResult.equals(getString(R.string.location_2), ignoreCase = true)
            -> {
                var ttsRequest: TtsRequest = TtsRequest.create(
                    getString(R.string.please_follow_asr) + getString(R.string.location_2),
                    true,
                    TtsRequest.Language.EN_US
                )
                ttsAsrUserLocation = ttsRequest.id.toString()
                this.setMessage(getString(R.string.location_2))
            }
            asrResult.toLowerCase(Locale.getDefault()).contains(getString(R.string.location_3))
                    || asrResult.equals(getString(R.string.location_3), ignoreCase = true)
            -> {
                var ttsRequest: TtsRequest = TtsRequest.create(
                    getString(R.string.please_follow_asr) + getString(R.string.location_3),
                    true,
                    TtsRequest.Language.EN_US
                )
                ttsAsrUserLocation = ttsRequest.id.toString()
                this.setMessage(getString(R.string.location_3))
            }
            asrResult.toLowerCase(Locale.getDefault()).contains(getString(R.string.location_4))
                    || asrResult.equals(getString(R.string.location_4), ignoreCase = true)
            -> {
                var ttsRequest: TtsRequest = TtsRequest.create(
                    getString(R.string.please_follow_asr) + getString(R.string.location_4),
                    true,
                    TtsRequest.Language.EN_US
                )
                ttsAsrUserLocation = ttsRequest.id.toString()
                this.setMessage(getString(R.string.location_4))
            }
            asrResult.toLowerCase(Locale.getDefault()).contains(getString(R.string.location_5))
                    || asrResult.equals(getString(R.string.location_5), ignoreCase = true)
            -> {
                var ttsRequest: TtsRequest = TtsRequest.create(
                    getString(R.string.please_follow_asr) + getString(R.string.location_5),
                    true,
                    TtsRequest.Language.EN_US
                )
                ttsAsrUserLocation = ttsRequest.id.toString()
                this.setMessage(getString(R.string.location_5))
            }
            asrResult.toLowerCase(Locale.getDefault()).contains(getString(R.string.location_6))
                    || asrResult.equals(getString(R.string.location_6), ignoreCase = true)
            -> {
                var ttsRequest: TtsRequest = TtsRequest.create(
                    getString(R.string.please_follow_asr) + getString(R.string.location_6),
                    true,
                    TtsRequest.Language.EN_US
                )
                ttsAsrUserLocation = ttsRequest.id.toString()
                this.setMessage(getString(R.string.location_6))
            }
            asrResult.toLowerCase(Locale.getDefault()).contains(getString(R.string.location_7))
                    || asrResult.equals(getString(R.string.location_7), ignoreCase = true)
            -> {
                var ttsRequest: TtsRequest = TtsRequest.create(
                    getString(R.string.please_follow_asr) + getString(R.string.location_7),
                    true,
                    TtsRequest.Language.EN_US
                )
                ttsAsrUserLocation = ttsRequest.id.toString()
                this.setMessage(getString(R.string.location_7))
            }
        }
    }

    override fun onGreetModeStateChanged(state: Int) {
        Log.i("onGreetStateChangedListener-- state: ", "$state")
        when (state) {
//            OnGreetModeStateChangedListener.GREETING
            3 -> {
                Log.i("onGreetStateChangedListener: ", "Greeting")
            }
//            OnGreetModeStateChangedListener.INTERACTION
            4 -> {
                Log.i("onGreetStateChangedListener: ", "Interaction")
                Handler().postDelayed({
                    robot?.askQuestion("Where would you like to go?")
                }, 2000)
            }
//            OnGreetModeStateChangedListener.ERROR
            -1 -> {
                Log.i("onGreetStateChangedListener: ", "Error")
            }
//            OnGreetModeStateChangedListener.HOLD
            0 -> {
                Log.i("onGreetStateChangedListener: ", "Hold")
            }
//            OnGreetModeStateChangedListener.POST_INTERACTION
            5 -> {
                Log.i("onGreetStateChangedListener: ", "Post Interaction")
            }
//            OnGreetModeStateChangedListener.PREPARING
            2 -> {
                Log.i("onGreetStateChangedListener: ", "Preparing")
            }
//            OnGreetModeStateChangedListener.SEARCHING
            1 -> {
                Log.i("onGreetStateChangedListener: ", "Searching")
            }
        }
    }
}
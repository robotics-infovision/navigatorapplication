package com.temi.navigator.notificationfull.service

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentValues
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.temi.navigation.R
import com.temi.navigator.notificationfull.activity.MainActivity
import com.temi.navigator.notificationfull.model.FirebaseData
import org.greenrobot.eventbus.EventBus
import java.util.*

@SuppressLint("MissingFirebaseInstanceTokenRefresh")
class MyFirebaseMessagingService : FirebaseMessagingService() {
    private val ADMIN_CHANNEL_ID = "admin_channel"//getString(R.string.admin_channel_id)
    @SuppressLint("LongLogTag", "UnspecifiedImmutableFlag")
    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.i(ContentValues.TAG, getString(R.string.log_from_firebase) + remoteMessage.from)

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.i(
            getString(R.string.log_onMessageRecieved), getString(R.string.log_title)
                    + remoteMessage.data[getString(R.string.key_title)] +" "
                    + getString(R.string.log_and_message)
                    + remoteMessage.data[getString(R.string.key_message)] + " "
                    + getString(R.string.log_and_name)
                    + remoteMessage.data[getString(R.string.key_name)]
        )

        val data_firebase : FirebaseData = FirebaseData("","","")
        data_firebase.setTitle(remoteMessage.data[getString(R.string.key_title)])
        data_firebase.setMessage(remoteMessage.data[getString(R.string.key_message)])
        data_firebase.setName(remoteMessage.data[getString(R.string.key_name)])
        EventBus.getDefault().post(data_firebase)

        val intent = Intent(this, MainActivity::class.java)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val notificationID = Random().nextInt(3000)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels(notificationManager)
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val largeIcon = BitmapFactory.decodeResource(resources, R.drawable.notify_icon)
        val notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
            .setSmallIcon(R.drawable.notify_icon)
            .setLargeIcon(largeIcon)
            .setContentTitle(remoteMessage.data[getString(R.string.key_title)])
            .setContentText(remoteMessage.data[getString(R.string.key_message)])
            .setAutoCancel(true)
            .setSound(notificationSoundUri)
            .setContentIntent(pendingIntent)

        //Set notification color to match your app color template
        notificationBuilder.color = resources.getColor(R.color.colorPrimaryDark)
        notificationManager.notify(notificationID, notificationBuilder.build())
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupChannels(notificationManager: NotificationManager?) {
        val adminChannelName: CharSequence = getString(R.string.admin_channel_name)
        val adminChannelDescription = getString(R.string.admin_channel_description)
        val adminChannel: NotificationChannel = NotificationChannel(
            ADMIN_CHANNEL_ID,
            adminChannelName,
            NotificationManager.IMPORTANCE_HIGH
        )
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.RED
        adminChannel.enableVibration(true)
        notificationManager?.createNotificationChannel(adminChannel)
    }

}
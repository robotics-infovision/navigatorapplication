package com.temi.navigation.notificationfull

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class MySingleton {
    private var instance: MySingleton? = null
    private var requestQueue: RequestQueue? = null
    private var ctx: Context? = null

    private fun MySingleton(context: Context) {
        ctx = context
        requestQueue = getRequestQueue()
    }

    @Synchronized
    fun getInstance(context: Context): MySingleton? {
        if (instance == null) {
            instance = this.getInstance(context)
        }
        return instance
    }

    private fun getRequestQueue(): RequestQueue {
        if (requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requestQueue = Volley.newRequestQueue(ctx!!.applicationContext)
        }
        return requestQueue!!
    }

    fun <T> addToRequestQueue(req: Request<T>?) {
        getRequestQueue().add(req)
    }
}

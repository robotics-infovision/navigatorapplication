package com.temi.navigation.constants

public class Constants {
    companion object{
        val C_FCM_API = "https://fcm.googleapis.com/fcm/send"
        val C_FBSKey = "AAAAL_d5Fi0:APA91bGdscia1Rfp1gWE5Bss8x01vvfn-bxsy-4EQbErxViq6VulLHgtksRG7VvNW5RYoabxHfaBTnLl82DesOkFiQ8cF3w6i5sTiw-oMykYL7vaoaHJbt9Dvi-NtOdwkVmJ_kCSaJSy"
        val C_serverKey = "key=$C_FBSKey"
        val C_contentType = "application/json"
        val C_TAG = "NOTIFICATION TAG"
        val C_key = "location"
        val C_name = "vinay"
        val C_greet_location = "reception"

        // Storage Permissions
        const val REQUEST_EXTERNAL_STORAGE = 1
        const val REQUEST_CODE_NORMAL = 0
        const val REQUEST_CODE_FACE_START = 1
        const val REQUEST_CODE_FACE_STOP = 2
        const val REQUEST_CODE_MAP = 3
        const val REQUEST_CODE_SEQUENCE_FETCH_ALL = 4
        const val REQUEST_CODE_SEQUENCE_PLAY = 5
        const val REQUEST_CODE_START_DETECTION_WITH_DISTANCE = 6
        const val REQUEST_CODE_SEQUENCE_PLAY_WITHOUT_PLAYER = 7
        const val REQUEST_CODE_GET_MAP_LIST = 8
    }
}